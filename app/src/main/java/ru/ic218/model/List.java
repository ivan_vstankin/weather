package ru.ic218.model;

import org.parceler.Parcel;

import java.util.ArrayList;

@Parcel
public class List
{
    int id;
    String name;
    Coord coord;
    Main main;
    int dt;
    Wind wind;
    Rain rain;
    Sys sys;
    Clouds clouds;
    String dt_txt;
    ArrayList<Weather> weather;

    public List() {

    }

    public List(int id, String name, Coord coord, Main main, int dt, Wind wind, Rain rain, Sys sys, Clouds clouds, String dt_txt, ArrayList<Weather> weather) {
        this.id = id;
        this.name = name;
        this.coord = coord;
        this.main = main;
        this.dt = dt;
        this.wind = wind;
        this.rain = rain;
        this.sys = sys;
        this.clouds = clouds;
        this.dt_txt = dt_txt;
        this.weather = weather;
    }

    public int getId() { return this.id; }

    public void setId(int id) { this.id = id; }

    public String getName() { return this.name; }

    public void setName(String name) { this.name = name; }

    public Coord getCoord() { return this.coord; }

    public void setCoord(Coord coord) { this.coord = coord; }

    public Main getMain() { return this.main; }

    public void setMain(Main main) { this.main = main; }

    public int getDt() { return this.dt; }

    public void setDt(int dt) { this.dt = dt; }

    public Wind getWind() { return this.wind; }

    public void setWind(Wind wind) { this.wind = wind; }

    public Rain getRain() { return this.rain; }

    public void setRain(Rain rain) { this.rain = rain; }

    public Sys getSys() { return this.sys; }

    public void setSys(Sys sys) { this.sys = sys; }

    public Clouds getClouds() { return this.clouds; }

    public void setClouds(Clouds clouds) { this.clouds = clouds; }

    public ArrayList<Weather> getWeather() { return this.weather; }

    public void setWeather(ArrayList<Weather> weather) { this.weather = weather; }

    public String getDtTxt() { return this.dt_txt; }

    public void setDtTxt(String dt_txt) { this.dt_txt = dt_txt; }

}
