package ru.ic218.model;

import org.parceler.Parcel;

@Parcel
public class Main
{
    double temp;
    double temp_min;
    double temp_max;
    double pressure;
    double sea_level;
    double grnd_level;
    int humidity;
    double temp_kf;

    public Main() {

    }

    public Main(double temp, double temp_min, double temp_max, double pressure, double sea_level, double grnd_level, int humidity, double temp_kf) {
        this.temp = temp;
        this.temp_min = temp_min;
        this.temp_max = temp_max;
        this.pressure = pressure;
        this.sea_level = sea_level;
        this.grnd_level = grnd_level;
        this.humidity = humidity;
        this.temp_kf = temp_kf;
    }

    public double getTemp() { return this.temp; }

    public void setTemp(double temp) { this.temp = temp; }

    public double getTempMin() { return this.temp_min; }

    public void setTempMin(double temp_min) { this.temp_min = temp_min; }

    public double getTempMax() { return this.temp_max; }

    public void setTempMax(double temp_max) { this.temp_max = temp_max; }

    public double getPressure() { return this.pressure; }

    public void setPressure(double pressure) { this.pressure = pressure; }

    public double getSeaLevel() { return this.sea_level * 0.75; }

    public void setSeaLevel(double sea_level) { this.sea_level = sea_level; }

    public double getGrndLevel() { return this.grnd_level * 0.75; }

    public void setGrndLevel(double grnd_level) { this.grnd_level = grnd_level; }

    public int getHumidity() { return this.humidity; }

    public void setHumidity(int humidity) { this.humidity = humidity; }

    public double getTempKf() { return this.temp_kf; }

    public void setTempKf(double temp_kf) { this.temp_kf = temp_kf; }
}
