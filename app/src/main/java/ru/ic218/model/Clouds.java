package ru.ic218.model;

import org.parceler.Parcel;

@Parcel
public class Clouds
{
    int all;

    public Clouds() {
    }

    public Clouds(int all) {
        this.all = all;
    }

    public String getAll() { return String.valueOf(this.all) + "%"; }

    public void setAll(int all) { this.all = all; }
}
