package ru.ic218.model;

import org.parceler.Parcel;

@Parcel
public class Sys
{
    String country;
    String pod;

    public Sys() {

    }

    public Sys(String country, String pod) {
        this.country = country;
        this.pod = pod;
    }

    public String getCountry() { return this.country; }

    public void setCountry(String country) { this.country = country; }

    public String getPod() { return this.pod; }

    public void setPod(String pod) { this.pod = pod; }
}
