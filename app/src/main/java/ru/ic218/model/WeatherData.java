package ru.ic218.model;

import org.parceler.Parcel;

import java.util.ArrayList;

@Parcel
public class WeatherData {

    City city;
    int cnt;
    String message;
    String cod;
    int count;
    ArrayList<List> list;

    public WeatherData() {

    }

    public WeatherData(City city, int cnt, String message, String cod, int count, ArrayList<List> list) {
        this.city = city;
        this.cnt = cnt;
        this.message = message;
        this.cod = cod;
        this.count = count;
        this.list = list;
    }

    public City getCity() { return this.city; }

    public void setCity(City city) { this.city = city; }

    public int getCnt() { return this.cnt; }

    public void setCnt(int cnt) { this.cnt = cnt; }

    public String getMessage() { return this.message; }

    public void setMessage(String message) { this.message = message; }

    public String getCod() { return this.cod; }

    public void setCod(String cod) { this.cod = cod; }

    public int getCount() { return this.count; }

    public void setCount(int count) { this.count = count; }

    public ArrayList<List> getList() { return this.list; }

    public void setList(ArrayList<List> list) { this.list = list; }

}
