package ru.ic218.model;

import org.parceler.Parcel;

@Parcel
public class Rain
{
    double h3;

    public Rain() {
    }

    public Rain(double h3) {
        this.h3 = h3;
    }

    public double get3h() { return this.h3; }

    public void set3h(double h3) { this.h3 = h3; }
}
