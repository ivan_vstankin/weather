package ru.ic218.model;

import org.parceler.Parcel;

@Parcel
public class Wind
{
    double speed;
    double deg;

    public Wind() {
    }

    public Wind(double speed, double deg) {
        this.speed = speed;
        this.deg = deg;
    }

    public String getSpeed() { return String.valueOf(this.speed) + " m\\s"; }

    public void setSpeed(double speed) { this.speed = speed; }

    public double getDeg() { return this.deg; }

    public void setDeg(double deg) { this.deg = deg; }
}
