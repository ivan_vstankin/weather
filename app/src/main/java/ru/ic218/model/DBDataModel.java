package ru.ic218.model;

import io.realm.RealmObject;

public class DBDataModel extends RealmObject {

    private String cityName;
    private String temp;
    private String weatherMain;
    private String windSpeed;
    private String clouds;
    private String humidity;
    private String atmPressureSea;
    private String atmPressureGround;
    private String dateUpdate;

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getWeatherMain() {
        return weatherMain;
    }

    public void setWeatherMain(String weatherMain) {
        this.weatherMain = weatherMain;
    }

    public String getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(String windSpeed) {
        this.windSpeed = windSpeed;
    }

    public String getClouds() {
        return clouds;
    }

    public void setClouds(String clouds) {
        this.clouds = clouds;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getAtmPressureSea() {
        return atmPressureSea;
    }

    public void setAtmPressureSea(String atmPressureSea) {
        this.atmPressureSea = atmPressureSea;
    }

    public String getAtmPressureGround() {
        return atmPressureGround;
    }

    public void setAtmPressureGround(String atmPressureGround) {
        this.atmPressureGround = atmPressureGround;
    }

    public String getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(String dateUpdate) {
        this.dateUpdate = dateUpdate;
    }
}
