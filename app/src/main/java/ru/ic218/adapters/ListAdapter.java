package ru.ic218.adapters;

import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import ru.ic218.weather.R;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    private String[] mArray;
    private clickOnList mItemClick;

    public ListAdapter(String[] array, clickOnList click) {
        this.mArray = array;
        this.mItemClick = click;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.name) TextView mText;
        @BindView(R.id.rootLayout) LinearLayout mLayout;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    @Override
    public int getItemCount() {
        return mArray.length;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        final int mItemClickPosition = holder.getAdapterPosition();

        holder.mText.setText(mArray[mItemClickPosition]);

        holder.mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemClick.onClick(mItemClickPosition);
            }
        });

        holder.mLayout.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                mItemClick.onLongClick(mItemClickPosition);
                return false;
            }
        });
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View cv = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item, parent, false);

        return new ViewHolder(cv);
    }

    public interface clickOnList
    {
        void onClick(int position);

        void onLongClick(int position);
    }

}
