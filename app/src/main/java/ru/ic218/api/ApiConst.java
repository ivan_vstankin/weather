package ru.ic218.api;

public class ApiConst {

    public static final String BASE_URL = "http://api.openweathermap.org/data/2.5/";
    public static final String API_ID = "dcd8e6a882972be0051064755ecbf85d";
    public static final String CURRENT_WEATHER_PATH = "find?units=metric&mode=json&lang=en&appid=" + API_ID;

    public static final String DAY5_WEATHER_PATH = "forecast?units=metric&mode=json&lang=en&appid=" + API_ID;
}

