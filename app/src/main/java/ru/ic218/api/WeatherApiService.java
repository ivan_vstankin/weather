package ru.ic218.api;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.ic218.model.WeatherData;

public interface WeatherApiService {

    @GET(ApiConst.CURRENT_WEATHER_PATH)
    Call<WeatherData> weatherData(@Query("lat") double lat, @Query("lon") double lon);

    @GET(ApiConst.DAY5_WEATHER_PATH)
    Call<WeatherData> weatherData5Days(@Query("lat") double lat, @Query("lon") double lon);
}
