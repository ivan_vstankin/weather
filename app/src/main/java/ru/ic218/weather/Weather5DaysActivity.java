package ru.ic218.weather;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.ic218.api.ApiConst;
import ru.ic218.api.WeatherApiService;
import ru.ic218.model.List;
import ru.ic218.model.WeatherData;

public class Weather5DaysActivity extends AppCompatActivity {

    public static final String MY_INTENT_LAT = "ru.ic218.weather.5days.intent.lat";
    public static final String MY_INTENT_LON = "ru.ic218.weather.5days.intent.lot";

    @BindView(R.id.graph_temp) GraphView graph_temp;
    @BindView(R.id.graph_pressure) GraphView graph_pressure;
    @BindView(R.id.graph_humidity) GraphView graph_humidity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather5_days);

        ButterKnife.bind(this);

        double lat = getIntent().getDoubleExtra(MY_INTENT_LAT,0);
        double lon = getIntent().getDoubleExtra(MY_INTENT_LON,0);

        getWeatherByLocation5Day(lat,lon);

    }

    private void getWeatherByLocation5Day(double lat, double lon){
        Retrofit client = new Retrofit.Builder()
                .baseUrl(ApiConst.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        WeatherApiService service = client.create(WeatherApiService.class);
        Call<WeatherData> call = service.weatherData5Days(lat, lon);
        call.enqueue(mCallback);
    }

    Callback<WeatherData> mCallback = new Callback<WeatherData>() {
        @Override
        public void onResponse(Call<WeatherData> call, Response<WeatherData> response) {
            if (response.isSuccessful()) {
                initTempGraphView(response.body().getList());
                initPressureGraphView(response.body().getList());
                initHumidityGraphView(response.body().getList());
            }
        }

        @Override
        public void onFailure(Call<WeatherData> call, Throwable t) {
            Toast.makeText(getBaseContext(),t.getMessage(),Toast.LENGTH_SHORT).show();
        }
    };


    private void initTempGraphView(ArrayList<List> mArray){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat df = new SimpleDateFormat("dd-MM HH:mm");
        Date dateMinX = new Date();

        DataPoint[] dp = new DataPoint[mArray.size()];

        java.util.List<String> iList = new ArrayList<>();

        int step = Math.round(mArray.size()/2);
        int iMod = 0;

        Date date = new Date();
        for (int i = 0; i < mArray.size(); i++) {

            List item =  mArray.get(i);

            try {
                date = sdf.parse(item.getDtTxt());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            String formattedDate = df.format(date);

            if (i == 0){
                dateMinX = date;
            }

            if (iMod == i && iMod < mArray.size()){
                iList.add(formattedDate);
                iMod += step;
            }

            dp[i] = new DataPoint(date, item.getMain().getTemp());

            //dateFormat[i] = formattedDate;
        }

        String[] dateFormat = new String[iList.size()];
        iList.toArray(dateFormat);

        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(dp);
        series.setTitle("temp");

        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph_temp);
        staticLabelsFormatter.setHorizontalLabels(dateFormat);

        graph_temp.addSeries(series);
        graph_temp.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        graph_temp.getLegendRenderer().setVisible(true);
        graph_temp.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.BOTTOM);
        graph_temp.getViewport().setXAxisBoundsManual(true);
        graph_temp.getViewport().setMinX(dateMinX.getTime());
        graph_temp.getViewport().setMaxX(date.getTime());

    }

    private void initPressureGraphView(ArrayList<List> mArray){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat df = new SimpleDateFormat("dd-MM HH:mm");
        Date dateMinX = new Date();

        DataPoint[] dp = new DataPoint[mArray.size()];

        java.util.List<String> iList = new ArrayList<>();

        int step = Math.round(mArray.size()/2);
        int iMod = 0;

        Date date = new Date();
        for (int i = 0; i < mArray.size(); i++) {

            List item =  mArray.get(i);

            try {
                date = sdf.parse(item.getDtTxt());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            String formattedDate = df.format(date);

            if (i == 0){
                dateMinX = date;
            }

            if (iMod == i && iMod < mArray.size()){
                iList.add(formattedDate);
                iMod += step;
            }

            dp[i] = new DataPoint(date, item.getMain().getPressure()*0.75);
        }

        String[] dateFormat = new String[iList.size()];
        iList.toArray(dateFormat);

        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(dp);
        series.setTitle("pressure");

        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph_pressure);
        staticLabelsFormatter.setHorizontalLabels(dateFormat);

        graph_pressure.addSeries(series);
        graph_pressure.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        graph_pressure.getLegendRenderer().setVisible(true);
        graph_pressure.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.BOTTOM);
        graph_pressure.getViewport().setXAxisBoundsManual(true);
        graph_pressure.getViewport().setMinX(dateMinX.getTime());
        graph_pressure.getViewport().setMaxX(date.getTime());
    }

    private void initHumidityGraphView(ArrayList<List> mArray){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat df = new SimpleDateFormat("dd-MM HH:mm");
        Date dateMinX = new Date();

        DataPoint[] dp = new DataPoint[mArray.size()];

        java.util.List<String> iList = new ArrayList<>();

        int step = Math.round(mArray.size()/2);
        int iMod = 0;

        Date date = new Date();
        for (int i = 0; i < mArray.size(); i++) {

            List item =  mArray.get(i);

            try {
                date = sdf.parse(item.getDtTxt());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            String formattedDate = df.format(date);

            if (i == 0){
                dateMinX = date;
            }

            if (iMod == i && iMod < mArray.size()){
                iList.add(formattedDate);
                iMod += step;
            }

            dp[i] = new DataPoint(date, item.getMain().getHumidity());
        }

        String[] dateFormat = new String[iList.size()];
        iList.toArray(dateFormat);

        LineGraphSeries<DataPoint> series = new LineGraphSeries<>(dp);
        series.setTitle("humidity");

        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph_humidity);
        staticLabelsFormatter.setHorizontalLabels(dateFormat);

        graph_humidity.addSeries(series);
        graph_humidity.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        graph_humidity.getLegendRenderer().setVisible(true);
        graph_humidity.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.BOTTOM);
        graph_humidity.getViewport().setXAxisBoundsManual(true);
        graph_humidity.getViewport().setMinX(dateMinX.getTime());
        graph_humidity.getViewport().setMaxX(date.getTime());
    }
}
