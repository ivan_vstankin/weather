package ru.ic218.weather;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import icepick.Icepick;
import icepick.State;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.ic218.adapters.ListAdapter;
import ru.ic218.api.ApiConst;
import ru.ic218.model.DBDataModel;
import ru.ic218.model.List;
import ru.ic218.model.WeatherData;
import ru.ic218.api.WeatherApiService;
import ru.ic218.service.WeatherListener;
import ru.ic218.service.WeatherManagerService;

public class MainActivity extends AppCompatActivity{

    private static final int MY_PERMISSIONS_REQUEST_GPS = 1;
    private static final int MY_PERMISSIONS_REQUEST_NETWORK = 2;

    public static boolean MY_PERMISSIONS_GPS = false;
    public static boolean MY_PERMISSIONS_NETWORK = false;

    public static final String APP_PREFERENCES = "mySettings";
    public static final String APP_PREFERENCES_SEEK_VALUE = "seekValue";

    public static final String APP_PREFERENCES_LATITUTE_VALUE = "latitute";
    public static final String APP_PREFERENCES_LONGITUTE_VALUE = "longitute";

    public static int seekValue = 0;

    @State(MyBundler.class) protected WeatherData mWeatherData;

    private WeatherManagerService mWeatherManagerService;

    @BindView(R.id.lvMain) RecyclerView lvMain;
    @BindView(R.id.cityName) TextView cityName;
    @BindView(R.id.dateUpdate) TextView dateUpdate;
    @BindView(R.id.txtTemp) TextView temp;
    @BindView(R.id.weather_main) TextView weatherMain;
    @BindView(R.id.windSpeed) TextView windSpeed;
    @BindView(R.id.clouds) TextView clouds;
    @BindView(R.id.humidity) TextView humidity;
    @BindView(R.id.atmPressureSea) TextView atmPressureSea;
    @BindView(R.id.atmPressureGround) TextView atmPressureGround;

    private boolean hasLocation = false;
    private Location currentLocation;
    private Integer selectedItemListView;
    private SharedPreferences mSettings;
    private String selectedCurrentCity;

    private Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //-----Получение сохраненных настроек-------
        mSettings = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        if(mSettings.contains(APP_PREFERENCES_SEEK_VALUE)) {
            seekValue = mSettings.getInt(APP_PREFERENCES_SEEK_VALUE, 0);
        }
        //------------------------------------------

        ButterKnife.bind(this);

        lvMain.setLayoutManager(new LinearLayoutManager(this));

        //-----Получение данных из кэша-------
        RealmResults<DBDataModel> item = initDatabaseRealm();
        if (item.size() > 0){
            getDBDataModel(item.last());
        }
        //------------------------------------------

        //-----Запрос полномочий для GPS и Network-------
        permissionRequest();
        //------------------------------------------

        //-----Инициализация класса управления сервисом-------
        mWeatherManagerService = WeatherManagerService.with(this);
        //----------------------------------------------------
    }

    @Override
    protected void onStart() {
        super.onStart();

        mWeatherManagerService.connect();
        mWeatherManagerService.registerListener(mWeatherListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (isFinishing()){
            mWeatherManagerService.unregisterListener(mWeatherListener);
            mWeatherManagerService.disconnect();
            setDBDataModel();
            realm.close();
        }
    }

    @Override public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Icepick.restoreInstanceState(this, savedInstanceState);

        if (mWeatherData != null) {
            setCurrentData(mWeatherData.getList().get(0));
        }
    }

    @Override public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Icepick.saveInstanceState(this, outState);
    }

    private RealmResults<DBDataModel> initDatabaseRealm(){

        // Create a RealmConfiguration that saves the Realm file in the app's "files" directory.
        RealmConfiguration realmConfig = new RealmConfiguration.Builder(getBaseContext()).build();
        Realm.setDefaultConfiguration(realmConfig);

        // Get a Realm instance for this thread
        realm = Realm.getDefaultInstance();

        // Query Realm
        return realm.where(DBDataModel.class).findAll();
    }

    /*
        * Функция получения местоположения.
    */
    private void runLocation(){
        LocationControl locationControlTask = new LocationControl();
        locationControlTask.execute(this);

        LocationHelper locHelper = new LocationHelper();
        locHelper.getLocation(getBaseContext(), locationResult);
    }

    /*
        * Функция получения данных с сервера погоды.
    */
    private void getWeatherByLocation(double lat, double lon){
        Retrofit client = new Retrofit.Builder()
                .baseUrl(ApiConst.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        WeatherApiService service = client.create(WeatherApiService.class);
        Call<WeatherData> call = service.weatherData(lat, lon);
        call.enqueue(mCallback);
    }

    Callback<WeatherData> mCallback = new Callback<WeatherData>() {
        @Override
        public void onResponse(Call<WeatherData> call, Response<WeatherData> response) {
            if (response.isSuccessful()) {
                //---При первом запуске mWeatherData == null в связи с этим мы получаем дополнительно перечень
                //---городов и загружаем их в список
                /*if (mWeatherData == null){*/
                    mWeatherData = response.body();
                    int arraySize = response.body().getList().size();
                    String[] arrayName = new String[arraySize];
                    for (int i = 0; i < arraySize; i++)
                    {
                        arrayName[i] = response.body().getList().get(i).getName();
                        Log.d("myLogs", "onResponse: " + String.valueOf(arrayName[i]));
                    }

                    ListAdapter adapter = new ListAdapter(arrayName, click);
                    lvMain.setAdapter(adapter);
                    Toast.makeText(getBaseContext(),"Получены данные с сервера погоды по текущему местоположению",Toast.LENGTH_SHORT).show();

                    if (selectedCurrentCity!=null){
                        for (List item : mWeatherData.getList()) {
                            if (selectedCurrentCity.equals(item.getName())){
                                setCurrentData(item);
                            }
                        }
                    }

/*                } else {
                    //---При последующих запусках мы обновляем только объект mWeatherData
                    //---а также последний выбранных город
                    mWeatherData = response.body();
                    if (selectedCurrentCity!=null){
                        for (List item : mWeatherData.getList()) {
                            if (selectedCurrentCity.equals(item.getName())){
                                setCurrentData(item);
                            }
                        }
                    }
                }*/
            } else {

            }
        }

        @Override
        public void onFailure(Call<WeatherData> call, Throwable t) {
            Toast.makeText(getBaseContext(),"Нет соединения с сервером погоды",Toast.LENGTH_SHORT).show();
        }
    };

    /*
       * Запуск AsyncTask который отображает диалоговое окно для ожидания получения местоположения, в случае успеха
       * запускается получение данных с сервера погоды.
     */
    private class LocationControl extends AsyncTask<Context, Void, Void>
    {
        private final ProgressDialog dialog = new ProgressDialog(MainActivity.this, R.style.Theme_MyDialog);

        protected void onPreExecute()
        {
            this.dialog.setMessage("Searching location");
            this.dialog.show();
        }

        protected Void doInBackground(Context... params)
        {
            //Wait 10 seconds to see if we can get a location from either network or GPS, otherwise stop
            Long t = Calendar.getInstance().getTimeInMillis();
            while (!hasLocation && Calendar.getInstance().getTimeInMillis() - t < 15000) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            };
            return null;
        }

        protected void onPostExecute(final Void unused)
        {
            if(this.dialog.isShowing())
            {
                this.dialog.dismiss();
            }

            if (currentLocation != null)
            {
                getWeatherByLocation(currentLocation.getLatitude(), currentLocation.getLongitude());
                Toast.makeText(getBaseContext(),"Местоположение определено",Toast.LENGTH_SHORT).show();
            }
            else
            {
                Toast.makeText(getBaseContext(),"Не удалось определить местоположение",Toast.LENGTH_SHORT).show();
            }
        }
    }

    /*
       * Результат получения местоположения.
     */
    public LocationHelper.LocationResult locationResult = new LocationHelper.LocationResult()
    {
        @Override
        public void gotLocation(final Location location)
        {
            if (location != null){
                currentLocation = new Location(location);
                hasLocation = true;
            }
        }
    };

    /*
       * Функция записи данных (сохранения в кэш) при закрытии приложения
     */
    private void setDBDataModel(){

        realm.beginTransaction();
        realm.deleteAll();

        DBDataModel dbDataModel = realm.createObject(DBDataModel.class);
        dbDataModel.setCityName(cityName.getText().toString());
        dbDataModel.setTemp(temp.getText().toString());
        dbDataModel.setWeatherMain(weatherMain.getText().toString());
        dbDataModel.setWindSpeed(windSpeed.getText().toString());
        dbDataModel.setClouds(clouds.getText().toString());
        dbDataModel.setHumidity(humidity.getText().toString());
        dbDataModel.setAtmPressureSea(atmPressureSea.getText().toString());
        dbDataModel.setAtmPressureGround(atmPressureGround.getText().toString());
        dbDataModel.setDateUpdate(dateUpdate.getText().toString());

        realm.commitTransaction();
    }

    /*
       * Функция чтения данных (из кэша) при открытии приложения
     */
    private void getDBDataModel(DBDataModel item){

        cityName.setText(item.getCityName());
        temp.setText(item.getTemp());
        weatherMain.setText(item.getWeatherMain());
        windSpeed.setText(item.getWindSpeed());
        clouds.setText(item.getClouds());
        humidity.setText(item.getHumidity());
        atmPressureSea.setText(item.getAtmPressureSea());
        atmPressureGround.setText(item.getAtmPressureGround());
        dateUpdate.setText(item.getDateUpdate());
    }


    /*
      * Обработчик нажатия на объект в RecycleView
    */
    private ListAdapter.clickOnList click = new ListAdapter.clickOnList() {
        @Override
        public void onClick(int position) {

            List data = mWeatherData.getList().get(position);

            selectedCurrentCity = data.getName();
            selectedItemListView = position;

            setCurrentData(data);

            doSaveSettingsLatLon((long)data.getCoord().getLat(),(long)data.getCoord().getLon());

        }

        @Override
        public void onLongClick(int position) {

            List data = mWeatherData.getList().get(position);

            Intent intent = new Intent(getBaseContext(), Weather5DaysActivity.class);
            intent.putExtra(Weather5DaysActivity.MY_INTENT_LAT,data.getCoord().getLat());
            intent.putExtra(Weather5DaysActivity.MY_INTENT_LON,data.getCoord().getLon());
            startActivity(intent);

        }
    };

    private void setCurrentData(List data){

        cityName.setText(data.getName());
        temp.setText(String.valueOf(data.getMain().getTemp()));
        weatherMain.setText(String.format(getString(R.string.string_format_1), data.getWeather().get(0).getMain(), data.getWeather().get(0).getDescription()));
        windSpeed.setText(data.getWind().getSpeed());
        clouds.setText(data.getClouds().getAll());
        humidity.setText(String.format(getString(R.string.string_format_2), data.getMain().getHumidity()));

        String formattedDoubleSea = new DecimalFormat("#0.00").format(data.getMain().getSeaLevel());

        atmPressureSea.setText(String.format(getString(R.string.string_format_3), formattedDoubleSea));

        String formattedDoubleGround = new DecimalFormat("#0.00").format(data.getMain().getGrndLevel());
        atmPressureGround.setText(String.format(getString(R.string.string_format_3), formattedDoubleGround));

        long unixSeconds = data.getDt();
        Date date = new Date(unixSeconds*1000L);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("GMT+7"));
        String formattedDate = sdf.format(date);
        dateUpdate.setText(String.format(getString(R.string.string_format_4), formattedDate));
    }

    private void permissionRequest(){

        if (!isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION, this)){

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                Toast.makeText(getBaseContext(),"Для определения местоположения, необходимы права на GPS",Toast.LENGTH_LONG).show();

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_GPS);
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_GPS);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            MY_PERMISSIONS_GPS = true;
            runLocation();
        }

        if (!isPermissionGranted(Manifest.permission.ACCESS_NETWORK_STATE, this)){

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_NETWORK_STATE)) {

                Toast.makeText(getBaseContext(),"Для определения местоположения, необходимы права на NETWORK",Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_NETWORK_STATE},
                        MY_PERMISSIONS_REQUEST_NETWORK);

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_NETWORK_STATE},
                        MY_PERMISSIONS_REQUEST_NETWORK);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            MY_PERMISSIONS_NETWORK = true;
            runLocation();
        }

    }

    public static boolean isPermissionGranted(String permission, Context context){
        return (ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode)
        {
            case MY_PERMISSIONS_REQUEST_GPS:
            {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    MY_PERMISSIONS_GPS = true;
                    runLocation();

                } else {

                    showDialog(this, 2);
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            case MY_PERMISSIONS_REQUEST_NETWORK:
            {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    MY_PERMISSIONS_NETWORK = true;
                    runLocation();

                } else {

                    showDialog(this, 2);
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    /*
       * Обработчик нажатия кнопки обновления.
    */
    public void fabClick(View view) {
        if (currentLocation != null && selectedItemListView != null)
        {
            //lvMain.findViewHolderForAdapterPosition(selectedItemListView).itemView.performClick();
            getWeatherByLocation(currentLocation.getLatitude(), currentLocation.getLongitude());
        }
    }

    /*
      * Обработчик нажатия кнопки настроек.
    */
    public void onClickButtonSettings(View view){

        showDialog(this, 1);
    }

    private void showDialog(final MainActivity activity, int ID){

        AlertDialog dialog = getDialog(activity,ID);
        if (dialog != null){
            dialog.show();
        }
    }
    /*
      * Метод вызываемый при нажатии кнопки ОК в диалоге настроек.
    */
    public void doSaveSettings(int value){

        seekValue = value;

        //---Сохраняем данные в SharedPreferences------
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putInt(APP_PREFERENCES_SEEK_VALUE, seekValue);
        editor.apply();
        //---------------------------------------------

        if (value != 0){
            mWeatherManagerService.startAutoUpdate(value);
        } else {
            mWeatherManagerService.stopAutoUpdate();
        }
    }

    public void doSaveSettingsLatLon(long lat, long lon){

        //---Сохраняем данные в SharedPreferences------
        SharedPreferences.Editor editor = mSettings.edit();
        editor.putLong(APP_PREFERENCES_LATITUTE_VALUE,lat);
        editor.putLong(APP_PREFERENCES_LONGITUTE_VALUE,lon);
        editor.apply();
        //---------------------------------------------
    }

    /*
      * Метод вызываемый при вызове notifyUpdate из службы. Вызывается при срабатывании AlarmManager.
    */
    private WeatherListener mWeatherListener = new WeatherListener() {
        @Override
        public void onWeatherUpdate() {
            Log.d("myLogs", "onWeatherUpdate");
            getWeatherByLocation(currentLocation.getLatitude(), currentLocation.getLongitude());
        }
    };

    public static AlertDialog getDialog(final MainActivity activity, int ID) {
        ContextThemeWrapper themeWrapper = new ContextThemeWrapper(activity,R.style.Theme_MyDialog);
        AlertDialog.Builder builder = new AlertDialog.Builder(themeWrapper);

        switch(ID) {
            case 1: {
                // Диалог настроек
                LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View view = inflater.inflate(R.layout.settings, (ViewGroup)null);

                final SeekBar sb_step = (SeekBar)view.findViewById(R.id.seekStep);
                final TextView txt_value = (TextView)view.findViewById(R.id.txtSeekValue);

                builder.setView(view);
                builder.setTitle(R.string.dialog_settings_title);
                builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener()
                { // Кнопка ОК
                    public void onClick(DialogInterface dialog, int whichButton) {

                        activity.doSaveSettings(sb_step.getProgress());
                        dialog.dismiss();
                    }
                });

                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener()
                { // Кнопка Отмена
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                builder.setCancelable(true);

                sb_step.setMax(120);
                sb_step.incrementProgressBy(5);
                sb_step.setProgress(MainActivity.seekValue);
                txt_value.setText(String.valueOf(MainActivity.seekValue));

                sb_step.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                        txt_value.setText(String.valueOf(seekBar.getProgress()));
                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {

                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                    }
                });

                return builder.create();
            }
            case 2:
            {

                builder.setTitle(R.string.dialog_permission_info);
                builder.setMessage(R.string.dialog_permission_info_message);
                return builder.create();

            }
            default:
                return null;
        }
    }
}
