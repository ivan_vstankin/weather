package ru.ic218.weather;

import android.os.Bundle;

import org.parceler.Parcels;

import icepick.Bundler;
import ru.ic218.model.WeatherData;

public class MyBundler implements Bundler<WeatherData> {
    @Override
    public void put(String s, WeatherData example, Bundle bundle) {
        bundle.putParcelable(s, Parcels.wrap(example));
    }

    @Override
    public WeatherData get(String s, Bundle bundle) {
        return Parcels.unwrap(bundle.getParcelable(s));
    }
}
