package ru.ic218.service;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.ic218.api.ApiConst;
import ru.ic218.api.WeatherApiService;
import ru.ic218.model.DBDataModel;
import ru.ic218.model.WeatherData;
import ru.ic218.weather.MainActivity;

public class WeatherService extends Service{


    private static final String INTENT_SCHEDULE = "ru.ic218.weather.INTENT_SCHEDULE";

    private final IBinder binder = new WeatherBinder();

    private AlarmManager mAlarmManager;
    List<WeatherListener> mListenerList;
    private PendingIntent mPendingIntent;
    private Realm realm;

    public WeatherService() {
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public class WeatherBinder extends Binder {
        public WeatherService getWeatherService() {
            return WeatherService.this;
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        String action = intent.getAction();

        if (action == null) return START_NOT_STICKY;

        if (action.equals(INTENT_SCHEDULE)) {
            notifyUpdate();
        }

        return START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        mListenerList = new ArrayList<>();

        mAlarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        Intent mIntent = new Intent(INTENT_SCHEDULE);
        mPendingIntent = PendingIntent.getService(this,0, mIntent,0);
    }

    public void startAutoUpdate(int interval)
    {
        mAlarmManager.setRepeating(AlarmManager.RTC,System.currentTimeMillis() + (60000*interval), 60000*interval,mPendingIntent);
    }

    public void stopAutoUpdate()
    {
        mAlarmManager.cancel(mPendingIntent);
    }

    public void registerListener(WeatherListener mListener) {
        mListenerList.add(mListener);
    }

    public void unregisterListener(WeatherListener mListener) {
        mListenerList.remove(mListener);
    }

    private void notifyUpdate() {

        SharedPreferences mSettings = getSharedPreferences(MainActivity.APP_PREFERENCES, Context.MODE_PRIVATE);
        if(mSettings.contains(MainActivity.APP_PREFERENCES_LATITUTE_VALUE) && mSettings.contains(MainActivity.APP_PREFERENCES_LONGITUTE_VALUE)) {
            long lat = mSettings.getLong(MainActivity.APP_PREFERENCES_LATITUTE_VALUE, 0);
            long lon = mSettings.getLong(MainActivity.APP_PREFERENCES_LONGITUTE_VALUE, 0);

            getWeatherByLocation(lat, lon);
        }
    }

    /*
        * Функция получения данных с сервера погоды.
    */
    private void getWeatherByLocation(double lat, double lon){
        Retrofit client = new Retrofit.Builder()
                .baseUrl(ApiConst.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        WeatherApiService service = client.create(WeatherApiService.class);
        Call<WeatherData> call = service.weatherData(lat, lon);
        call.enqueue(mCallback);
    }

    Callback<WeatherData> mCallback = new Callback<WeatherData>() {
        @Override
        public void onResponse(Call<WeatherData> call, Response<WeatherData> response) {
            if (response.isSuccessful()) {
                SetDBDataModel(response.body());

                //оставил обновление в случае активной активити
                for (WeatherListener mWeatherListener : mListenerList) {
                    mWeatherListener.onWeatherUpdate();
                }

                stopSelf();
            }
        }

        @Override
        public void onFailure(Call<WeatherData> call, Throwable t) {
            Toast.makeText(getBaseContext(),"Нет соединения с сервером погоды",Toast.LENGTH_SHORT).show();
        }
    };

    private RealmResults<DBDataModel> InitDatabaseRealm(){

        // Create a RealmConfiguration that saves the Realm file in the app's "files" directory.
        RealmConfiguration realmConfig = new RealmConfiguration.Builder(getBaseContext()).build();
        Realm.setDefaultConfiguration(realmConfig);

        // Get a Realm instance for this thread
        realm = Realm.getDefaultInstance();

        // Query Realm
        return realm.where(DBDataModel.class).findAll();
    }

    /*
      * Функция записи данных (сохранения в кэш) при закрытии приложения
    */
    private void SetDBDataModel(WeatherData mWeatherData){

        InitDatabaseRealm();

        if(mWeatherData != null){
            realm.beginTransaction();
            realm.deleteAll();

            DBDataModel dbDataModel = realm.createObject(DBDataModel.class);
            dbDataModel.setCityName(mWeatherData.getCity().toString());
            dbDataModel.setTemp(String.valueOf(mWeatherData.getList().get(0).getMain().getTemp()));
            dbDataModel.setWeatherMain(String.valueOf(mWeatherData.getList().get(0).getName()));
            dbDataModel.setWindSpeed(String.valueOf(mWeatherData.getList().get(0).getWind().getSpeed()));
            dbDataModel.setClouds(String.valueOf(mWeatherData.getList().get(0).getClouds().getAll()));
            dbDataModel.setHumidity(String.valueOf(mWeatherData.getList().get(0).getMain().getHumidity()));
            dbDataModel.setAtmPressureSea(String.valueOf(mWeatherData.getList().get(0).getMain().getSeaLevel()));
            dbDataModel.setAtmPressureGround(String.valueOf(mWeatherData.getList().get(0).getMain().getGrndLevel()));
            dbDataModel.setDateUpdate(mWeatherData.getList().get(0).getDtTxt());

            realm.commitTransaction();
        }
    }
}
