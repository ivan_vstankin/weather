package ru.ic218.service;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class WeatherManagerService  {

    private List<WeatherListener> mWeatherListenerQueue;
    private static WeatherManagerService instance = null;
    private WeatherService mService;
    private Context mContext;
    private boolean isServiceConnected;

    private WeatherManagerService(Context mContext) {
        this.mContext = mContext;
        mWeatherListenerQueue = new ArrayList<>();
        isServiceConnected = false;
    }

    public static WeatherManagerService with(Context mContext) {
        if (instance == null)
            instance = new WeatherManagerService(mContext);
        return instance;
    }

    public void registerListener(WeatherListener mWeatherListener) {

        if (isServiceConnected)
            mService.registerListener(mWeatherListener);
        else
            mWeatherListenerQueue.add(mWeatherListener);
    }

    public void unregisterListener(WeatherListener mWeatherListener) {

        mService.unregisterListener(mWeatherListener);
    }

    public void connect() {

        if (!isServiceConnected){
            log("Requested to connect service.");
            Intent intent = new Intent(mContext, WeatherService.class);
            mContext.bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);
        }
    }

    public void disconnect() {
        if (isServiceConnected){
            log("Requested to disconnect service.");
            mContext.unbindService(mServiceConnection);
            isServiceConnected = false;
        }
    }

    private ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName arg0, IBinder binder) {

            log("Service Connected.");
            mService = ((WeatherService.WeatherBinder) binder).getWeatherService();
            isServiceConnected = true;

            if (!mWeatherListenerQueue.isEmpty()) {
                for (WeatherListener mWeatherListener : mWeatherListenerQueue) {
                    registerListener(mWeatherListener);
                    //mWeatherListener.onWeatherUpdate();
                }
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mService = null;
            isServiceConnected = false;
        }
    };

    public void startAutoUpdate(int interval){
        if (mService != null){
            mService.startAutoUpdate(interval);
        }
    }

    public void stopAutoUpdate(){
        if (mService != null){
            mService.stopAutoUpdate();
        }
    }

    private void log(String log) {
            Log.v("myLogs", "ManagerLog : " + log);
    }
}
